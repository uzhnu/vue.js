class LocalstorageClass {
  static localstorageKey = "studentList";
  static editStudentToLocalstorage(student) {
    let list = this.read();
    console.log(localStorage.getItem(this.localstorageKey));
    const studentIndex = list.findIndex((item)=>item.id===student.id);
    list[studentIndex] = student;
    this.write(list);
  }
  static addStudentToLocalstorage(student) {
    let list = this.read()
    list.push(student)
    this.write(list)
  }
  static read(){
    return localStorage.getItem(this.localstorageKey)?JSON.parse(localStorage.getItem(this.localstorageKey)):[];
  }
  static write(list){
    console.log(list);
    console.log(this.localstorageKey);
    localStorage.setItem(this.localstorageKey, JSON.stringify(list));
  }
}
export default LocalstorageClass;
