import { createRouter, createWebHashHistory } from "vue-router";

import MainPage from "@/components/MainPage.vue";
import ContactPage from "@/components/ContactPage.vue";
import StudentAddPage from "@/components/StudentAddPage.vue";
import StudentPage from "@/components/StudentPage.vue";
import StudentEditPage from "@/components/StudentEditPage.vue";

const routes = [
  {
    path: "/",
    component: MainPage,
    name: "home",
  },
  {
    path: "/contact",
    component: ContactPage,
    name: "contact",
  },
  {
    path: "/student",
    component: StudentPage,
    name: "student",
  },
  {
    path: "/student/add",
    component: StudentAddPage,
    name: "student-add",
  },
  {
    path: "/student/edit",
    component: StudentEditPage,
    name: "student-edit",
  },
];

const router = createRouter({
  // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
  history: createWebHashHistory(),
  routes, // short for `routes: routes`
});
export default router;
