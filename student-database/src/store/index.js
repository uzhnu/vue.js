import { createStore } from "vuex";
import {defaultStudentList} from "./settings";
import LocalstorageClass from "@/localstorage";
const store = createStore({
  state() {
    return {
      studentList: [],
    };
  },
  getters: {
    studentList:({studentList})=>studentList,
    // getStudentByID:(state)=>((id)=>state.studentList.find((student)=> student.id == id)),
  },
  mutations: {
    // addNewStudent(state, newStudentObj) {
    //   state.studentList.push(newStudentObj);
    // },
    setStudentList(state, list){
      state.studentList = list;
    }
  },
  actions: {
    // addNewStudent({commit},newStudentObj) {
    //   commit("addNewStudent", newStudentObj);
    // },
    loadData({commit}){
      const studentListLocalstorage = LocalstorageClass.read();
      if(studentListLocalstorage.length){
        commit('setStudentList',studentListLocalstorage)
      } else{
        commit('setStudentList',defaultStudentList)
      }
    }
  },
});

export default store;
