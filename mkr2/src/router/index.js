import { createRouter, createWebHistory } from 'vue-router'
import MainView from '../views/MainView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: MainView
  },
  {
    path: '/:id',
    name: 'view',
    component: ()=> import('@/views/EquipmentView.vue')
  },
  {
    path: '/:id/edit',
    name: 'edit',
    component: ()=> import('@/views/EditView.vue')
  },
  {
    path: '/add',
    name: 'add',
    component: ()=> import('@/views/AddView.vue')
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('../views/AboutView.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
